<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.bibsonomy.jabref</groupId>
	<artifactId>jabref-bibsonomy-plugin</artifactId>
	<name>JabRef Bibsonomy Plug-in</name>
	<version>2.10.0</version>
	<description>
		Plugin for the reference management 
		software JabRef (http://jabref.sourceforge.net/) 
		to fetch, store and delete entries from BibSonomy.
	</description>
	<url>https:/bitbucket.org/bibsonomy/bibsonomy-jabref-plugin</url>
	<organization>
		<name>Knowledge and Data Engineering Group</name>
		<url>http://www.kde.cs.uni-kassel.de/</url>
	</organization>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>
	<inceptionYear>2008</inceptionYear>
	<licenses>
		<license>
			<name>GNU General Public License 2.0</name>
			<url>src/main/resources/GPL.txt</url>
		</license>
	</licenses>
	<mailingLists>
		<mailingList>
			<name>BibSononomy-Discuss: Für Benutzer von BibSonomy, insbesondere auch für auch Fragen zum JabRef-Plugin.</name>
			<subscribe>https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss</subscribe>
			<unsubscribe>https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss</unsubscribe>
			<post>bibsonomy-discuss@cs.uni-kassel.de</post>
			<archive>https://mail.cs.uni-kassel.de/pipermail/bibsonomy-discuss</archive>
		</mailingList>
		<mailingList>
			<name>Api-Discuss: Für Benutzer und Entwickler der BibSonomy-API.</name>
			<subscribe>https://mail.cs.uni-kassel.de/mailman/listinfo/api-discuss</subscribe>
			<unsubscribe>https://mail.cs.uni-kassel.de/mailman/listinfo/api-discuss</unsubscribe>
			<post>api-discuss@cs.uni-kassel.de</post>
			<archive>https://mail.cs.uni-kassel.de/pipermail/api-discuss</archive>
		</mailingList>
		<mailingList>
			<name>JabRef-Users: Allgemeine Liste für Benutzer von JabRef (betrieben von sourceforge.net)</name>
			<subscribe>https://lists.sourceforge.net/lists/listinfo/jabref-users</subscribe>
			<unsubscribe>https://lists.sourceforge.net/lists/listinfo/jabref-users</unsubscribe>
			<post>jabref-users@lists.sourceforge.net</post>
			<archive>http://sourceforge.net/mailarchive/forum.php?forum_name=jabref-users</archive>
		</mailingList>
	</mailingLists>
	<scm>
		<connection>scm:hg:ssh://hg@bitbucket.org/bibsonomy/bibsonomy-jabref-plugin</connection>
	</scm>
	<issueManagement>
		<system>Bitbucket</system>
		<url>https:/bitbucket.org/bibsonomy/bibsonomy-jabref-plugin/issues</url>
	</issueManagement>
	<ciManagement>
		<system>Jenkins</system>
		<url>http://jenkins.cs.uni-kassel.de/</url>
	</ciManagement>
	<repositories>
		<repository>
			<id>bibsonomy</id>
			<name>Releases von BibSonomy-Modulen</name>
			<url>http://dev.bibsonomy.org/maven2/</url>
		</repository>
		<repository>
			<id>ibiblio</id>
			<name>Ibiblio Apache Mirror</name>
			<url>http://www.ibiblio.org/maven/</url>
		</repository>
	</repositories>
	<developers>
		<developer>
			<id>boogie</id>
			<name>Waldemar Biller</name>
			<email>wbiller@gmail.com</email>
			<roles>
				<role>Developer</role>
			</roles>
		</developer>
		<developer>
			<name>Andreas Hotho</name>
			<id>aho</id>
			<email>hotho@cs.uni-kassel.de</email>
			<roles>
				<role>Project leader</role>
			</roles>
			<url>http://www.kde.cs.uni-kassel.de/hotho</url>
		</developer>
		<developer>
			<name>Dominik Benz</name>
			<id>dbe</id>
			<email>dbe@cs.uni-kassel.de</email>
			<roles>
				<role>Project leader</role>
				<role>Developer</role>
			</roles>
			<url>http://www.kde.cs.uni-kassel.de/benz</url>
		</developer>
		<developer>
			<name>Robert Jaeschke</name>
			<id>rja</id>
			<email>jaeschke@cs.uni-kassel.de</email>
			<roles>
				<role>Project leader</role>
				<role>Developer</role>
			</roles>
			<url>http://www.kde.cs.uni-kassel.de/jaeschke</url>
		</developer>
		<developer>
			<name>Beate Krause</name>
			<id>bkr</id>
			<email>krause@cs.uni-kassel.de</email>
			<roles>
				<role>Project leader</role>
				<role>Developer</role>
			</roles>
			<url>http://www.kde.cs.uni-kassel.de/krause</url>
		</developer>
	</developers>
	<distributionManagement>
		<site>
			<!-- copy site documentation via webdav to zope -->
			<id>website-bibsonomy-documentation</id>
			<url>dav:http://www.bibsonomy.org/help/doc/jabref-plugin/</url>
		</site>
	</distributionManagement>
	<build>
		<plugins>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
					<!-- <descriptors> <descriptor>src/main/assembly/jar-with-dependencies-excluding-jabref.xml</descriptor> 
						</descriptors> -->
					<finalName>
						${project.artifactId}-${project.version}-bin
					</finalName>
					<appendAssemblyId>false</appendAssemblyId>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>attached</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.0</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.google.code.maven-license-plugin</groupId>
				<artifactId>maven-license-plugin</artifactId>
				<version>1.3.1</version>
				<configuration>
					<header>src/etc/header.txt</header>
					<includes>
						<include>src/**</include>
						<include>**/test/**</include>
					</includes>
					<excludes>
						<exclude>target/**</exclude>
					</excludes>
					<properties>
						<year>${project.inceptionYear} - 2014</year>
						<description>
							${project.name} - ${project.description}
						</description>
					</properties>
				</configuration>
			</plugin>
			<plugin>
				<!-- generate a source jar during build pase and deploy it along with binary jar -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.1.2</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.1.1</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>JabRef</groupId>
			<artifactId>JabRef</artifactId>
			<version>2.10</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.bibsonomy</groupId>
			<artifactId>bibsonomy-rest-client</artifactId>
			<version>3.2.1</version>
		</dependency>
	</dependencies>
</project>

